import { createStackNavigator, createAppContainer } from 'react-navigation'

import HomeScreen from './screens/HomeScreen'
import MedScreen from './screens/MedScreen'
import InfoScreen from './screens/InfoScreen'

const App = createStackNavigator(
  {
    Home: { screen: HomeScreen },
    Meditation: { screen: MedScreen },
    Info: { screen: InfoScreen },
  },
  {
    initialRouteName: 'Home',
  }
)

export default createAppContainer(App)
