import React, { PureComponent } from 'react'
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  Linking,
  ScrollView,
  TouchableOpacity,
} from 'react-native'
import { Header, Icon } from 'react-native-elements'
import FooterAd from './components/FooterAd'

import { marginArticleVertical, marginArticleHorisontal } from './constants'
import { general, formatText } from './styles'

const medScreen = require('../assets/MedScreen.png')
const logo = require('../assets/logo/HereAndNowLogo.png')
const maxatmaLabLogoWhite = require('../assets/logo/MaxatmaLabWhite.png')

const article = [
  { id: 1, style: 'title', paragraph: 'О приложении' },
  {
    id: 2,
    style: 'usual',
    paragraph:
      'Осознавание — фундаментальная способность сознания присутствовать в точке реальности "здесь и сейчас".',
  },
  {
    id: 3,
    style: 'usual',
    paragraph:
      'Медитация — род психических упражнений по управлению вниманием, направленных на достижение гармонии в различных аспектах личности и качества жизни в целом.',
  },
  {
    id: 4,
    style: 'usual',
    paragraph:
      'В этом приложении представлены медитации на разные случаи. Выполнять их следует, когда сочтёшь нужным, но без фанатизма.',
  },
  {
    id: 5,
    style: 'quote',
    paragraph:
      'Сначала прими — потом действуй. Чтобы ни содержал настоящий момент, прими его так, как будто ты его выбрал. Всегда работай с ним, а не против него. Сделай его своим другом и союзником, а не врагом.',
    comment: '— Экхарт Толле',
  },
]

export default class MedScreen extends PureComponent {
  static navigationOptions = {
    header: null,
  }

  render() {
    // eslint-disable-next-line react/destructuring-assignment
    const { navigate } = this.props.navigation

    return (
      <ImageBackground
        source={medScreen}
        style={styles.container}
        imageStyle={{ height: 180, opacity: 0.5 }}
      >
        <Header
          statusBarProps={{
            translucent: true,
            barStyle: 'light-content',
            backgroundColor: 'transparent',
          }}
          backgroundColor="transparent"
          barStyle="light-content"
          // leftComponent={{ icon: 'menu', color: '#fff' }}
          leftComponent={<Logo _onPress={() => navigate('Home')} />}
          // rightComponent={
          //   <InfoButton onPress={() => navigation.navigate('Info')} />
          // }
          containerStyle={styles.header}
        />
        <ScrollView style={styles.body}>
          {article.map(textBlock => (
            <Text style={styles[textBlock.style]} key={textBlock.id}>
              {textBlock.paragraph}
              {textBlock.comment && (
                <Text style={styles.comment}>
                  {'\n\n'}
                  {textBlock.comment}
                </Text>
              )}
            </Text>
          ))}
        </ScrollView>
        <View style={styles.contacts}>
          <TouchableOpacity onPress={() => navigate('Home')}>
            <Image source={maxatmaLabLogoWhite} style={styles.maxatmalab} />
          </TouchableOpacity>
          <View style={styles.socIcons}>
            <Icon
              raised
              name="vk"
              type="font-awesome"
              color="#0E2460"
              onPress={() =>
                Linking.openURL(
                  'https://vk.com/maxatmalab#Intent;package=com.vkontakte.android;scheme=vkontakte;end'
                )
              }
            />
            <Icon
              raised
              name="instagram"
              type="font-awesome"
              color="#f50"
              onPress={() =>
                Linking.openURL(
                  'https://instagram.com/_u/maxatmalab/#Intent;package=com.instagram.android;scheme=https;end?hl=ru'
                )
              }
            />
          </View>
        </View>
        <FooterAd stylext={styles.footer} />
      </ImageBackground>
    )
  }
}

const Logo = ({ _onPress }) => (
  <TouchableOpacity style={styles.logo} onPress={_onPress}>
    <Image source={logo} style={styles.logoImg} />
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  ...general,
  ...formatText,
  contacts: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    textAlign: 'center',
    marginLeft: marginArticleHorisontal,
    marginRight: marginArticleHorisontal,
    marginBottom: marginArticleVertical,
    marginTop: marginArticleVertical,
  },
  maxatmalab: {
    color: '#fff',
    fontSize: 24,
    width: 125,
    height: 30,
  },
  socIcons: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    textAlign: 'center',
  },
})
