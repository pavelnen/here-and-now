/* eslint-disable import/prefer-default-export */
import {
  marginTitle,
  marginArticleVertical,
  marginArticleHorisontal,
} from './constants'

export const backColorDark = '#281e43' //'#1A0534'
export const backColorLight = '#6b668c' // '#766985'
export const textColorLight = '#fff'

export const general = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: backColorDark,
  },
  header: {
    height: 80,
    // paddingTop: 24,
    paddingBottom: 0,
    // flex: 1,
    // width: '100%',
    borderBottomWidth: 0,
    // opacity: 0,
  },
  logo: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  logoImg: {
    width: 512 * 0.4,
    height: 52 * 0.4,
    marginTop: 3,
    marginLeft: 0,
  },
  sublogo: {
    color: '#fff',
    fontFamily: 'Roboto-Light',
    fontWeight: 'normal',
    fontSize: 12,
  },
  infoView: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  infoButton: {
    height: 32,
    width: 32,
    marginRight: 15,
  },
  body: {
    flex: 1,
    // paddingTop: paddingPage,
    flexWrap: 'wrap',
    // justifyContent: 'flex-start',
    alignContent: 'center',
  },
  footer: {
    height: 50,
    backgroundColor: '#eef',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
}

export const formatText = {
  title: {
    textAlign: 'center',
    color: textColorLight,
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: marginTitle,
    marginBottom: marginTitle,
  },
  subtitle: {
    textAlign: 'left',
    color: textColorLight,
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: marginTitle * 0.8,
    marginBottom: marginTitle * 0.8,
    marginLeft: marginArticleHorisontal,
  },
  usual: {
    textAlign: 'left',
    color: textColorLight,
    fontSize: 18,
    fontFamily: 'Roboto-Light',
    fontWeight: 'normal',
    //lineHeight: 23,
    marginLeft: marginArticleHorisontal,
    marginRight: marginArticleHorisontal,
    marginTop: marginArticleVertical,
    marginBottom: marginArticleVertical,
  },
  quote: {
    textAlign: 'left',
    color: textColorLight,
    fontFamily: 'Roboto-Light',
    fontSize: 18,
    fontStyle: 'italic',
    marginLeft: marginArticleHorisontal,
    marginRight: marginArticleHorisontal,
    marginTop: marginArticleVertical * 4,
    marginBottom: marginArticleVertical * 4,
    paddingLeft: 20,
    borderLeftWidth: 2,
    borderColor: 'white',
  },
  definition: {
    textAlign: 'left',
    color: textColorLight,
    fontFamily: 'Roboto-Light',
    fontStyle: 'italic',
    fontSize: 18,
    marginLeft: marginArticleHorisontal,
    marginRight: marginArticleHorisontal,
    marginTop: marginArticleVertical,
    marginBottom: marginArticleVertical,
  },
  comment: {
    textAlign: 'right',
    // fontStyle: 'italic',
    fontSize: 14,
    color: '#ccc',
    width: '100%',
    paddingTop: 10,
  },
}
