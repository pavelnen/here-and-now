import React, { PureComponent } from 'react'
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  ImageBackground,
  Linking,
} from 'react-native'
import { Header, Icon } from 'react-native-elements'
import FooterAd from './components/FooterAd'

import { general, formatText } from './styles'
import { instructions } from './MedScreen/instructions'
import { marginArticleVertical, marginArticleHorisontal } from './constants'

const medScreen = require('../assets/MedScreen.png')
const logo = require('../assets/logo/HereAndNowLogo.png')
const infoIcon = require('../assets/infoButton.png')
const maxatmaLabLogoWhite = require('../assets/logo/MaxatmaLabWhite.png')

export default class MedScreen extends PureComponent {
  static navigationOptions = {
    header: null,
  }

  render() {
    // eslint-disable-next-line react/destructuring-assignment
    const { navigation } = this.props
    const key = navigation.getParam('key', 'General')

    return (
      <ImageBackground
        source={medScreen}
        style={styles.container}
        imageStyle={{ height: 180, opacity: 0.5 }}
      >
        <Header
          statusBarProps={{
            translucent: true,
            barStyle: 'light-content',
            backgroundColor: 'transparent',
          }}
          backgroundColor="transparent"
          barStyle="light-content"
          // leftComponent={{ icon: 'menu', color: '#fff' }}
          leftComponent={<Logo _onPress={() => navigation.navigate('Home')} />}
          rightComponent={
            <InfoButton _onPressInfo={() => navigation.navigate('Info')} />
          }
          containerStyle={styles.header}
        />
        <ScrollView style={styles.body} indicatorStyle="white">
          {instructions[key].text.map((textBlock, i) => (
            <Text style={styles[textBlock.style]} key={i}>
              {textBlock.paragraph}
              {textBlock.comment && (
                <Text style={styles.comment}>
                  {'\n\n'}
                  {textBlock.comment}
                </Text>
              )}
            </Text>
          ))}
          {instructions[key].author === 'maxatmalab' ? (
            <View style={styles.contacts}>
              <TouchableOpacity onPress={() => navigation.navigate('Home')}>
                <Image source={maxatmaLabLogoWhite} style={styles.maxatmalab} />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.contacts}>
              <Text style={styles.announcement}>
                {'Медитация от\n'}
                <Text style={styles.is}>Академии Интеграл Спейс</Text>
              </Text>
              <View style={styles.socIcons}>
                <Icon
                  raised
                  name="vk"
                  type="font-awesome"
                  color="#0E2460"
                  onPress={() =>
                    Linking.openURL(
                      'https://vk.com/integral_space#Intent;package=com.vkontakte.android;scheme=vkontakte;end'
                    )
                  }
                />
                <Icon
                  raised
                  name="instagram"
                  type="font-awesome"
                  color="#f50"
                  onPress={() =>
                    Linking.openURL(
                      'https://instagram.com/_u/integral_space/#Intent;package=com.instagram.android;scheme=https;end?hl=ru'
                    )
                  }
                />
              </View>
            </View>
          )}
        </ScrollView>
        <FooterAd stylext={styles.footer} />
      </ImageBackground>
    )
  }
}

const Logo = ({ _onPress }) => (
  <TouchableOpacity style={styles.logo} onPress={_onPress}>
    <Image source={logo} style={styles.logoImg} />
  </TouchableOpacity>
)

const InfoButton = ({ _onPressInfo }) => (
  <TouchableOpacity style={styles.infoView} onPress={_onPressInfo}>
    <Image source={infoIcon} style={styles.infoButton} />
  </TouchableOpacity>
)

const styles = StyleSheet.create({
  ...general,
  ...formatText,
  maxatmalab: {
    color: '#fff',
    fontSize: 24,
    width: 125,
    height: 30,
  },
  announcement: {
    color: '#fff',
    fontSize: 14,
    width: 150,
    // height: 30,
  },
  is: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
    width: 125,
    height: 30,
  },
  socIcons: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    textAlign: 'center',
  },
  contacts: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    textAlign: 'center',
    marginLeft: marginArticleHorisontal,
    marginRight: marginArticleHorisontal,
    marginBottom: marginArticleVertical * 2,
    marginTop: marginArticleVertical * 4,
  },
})
