import React, { Component as PureComponent } from 'react'
import { View, Image } from 'react-native'
import { AdMobBanner } from 'react-native-admob'

const maxatmaLabLogoBlack = require('./MaxatmaLabBlack.png')

export default class FooterAd extends PureComponent {
  render() {
    const { stylext } = this.props
    return (
      <View style={stylext}>
        {this.bannerError ? (
          <Image
            source={maxatmaLabLogoBlack}
            style={{ height: 30, width: 125 }}
          />
        ) : (
          <AdMobBanner
            adSize="smartBanner"
            adUnitID="ca-app-pub-7445351629806598/4152902234" // for prod
            // adUnitID="ca-app-pub-3940256099942544/6300978111" // for tests
            testDevices={[AdMobBanner.simulatorId]}
            onAdFailedToLoad={() => {
              this.bannerError = true
            }}
          />
        )}
      </View>
    )
  }
}
