import React, { PureComponent } from 'react'
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList,
} from 'react-native'
import { Header, ListItem } from 'react-native-elements'
import FooterAd from './components/FooterAd'
import { general, backColorDark, backColorLight } from './styles'

const logo = require('../assets/logo/HereAndNowLogo.png')
const infoIcon = require('../assets/infoButton.png')

const ava1 = require('../assets/ava1.png')
const ava2 = require('../assets/ava2.png')
const ava3 = require('../assets/ava3.png')
const ava4 = require('../assets/ava4.png')

const screenWidth = Dimensions.get('window').width

const list = [
  {
    name: 'Дыхание',
    avatar: ava1,
    subtitle:
      'Универсальная медитация, можно выполнять в любое время, в любом месте.',
    key: 'General',
    icon: { name: 'user', type: 'font-awesome' },
  },
  {
    name: 'Антистресс',
    avatar: ava2,
    subtitle: 'Управляй своими эмоциями',
    key: 'Emotions',
    icon: { name: 'user', type: 'font-awesome' },
  },
  {
    name: 'Звуки и Мысли',
    avatar: ava3,
    subtitle: 'Найди покой среди хаоса повседневности',
    key: 'Sounds',
    icon: { name: 'user', type: 'font-awesome' },
  },
  {
    name: 'Спокойной ночи',
    avatar: ava4,
    subtitle: 'Заверши день правильно',
    key: 'Night',
    icon: { name: 'user', type: 'font-awesome' },
  },
]

export default class HomeScreen extends PureComponent {
  static navigationOptions = {
    header: null,
  }

  keyExtractor = (item, index) => index.toString()

  renderItem = ({ item }) => (
    <ListItem
      title={item.name}
      subtitle={item.subtitle}
      leftAvatar={{
        rounded: false,
        size: 70,
        source: item.avatar,
        title: item.name[0],
        placeholderStyle: { borderRadius: 30 }
      }}
      containerStyle={styles.listItem}
      titleStyle={{ color: 'white', fontWeight: 'bold' }}
      subtitleStyle={{ color: 'white', fontFamily: 'Roboto-Light' }}
      onPress={() =>
        // eslint-disable-next-line react/destructuring-assignment
        this.props.navigation.navigate('Meditation', {
          key: item.key,
        })
      }
    />
  )

  render() {
    // eslint-disable-next-line react/destructuring-assignment
    const { navigate } = this.props.navigation

    return (
      <View style={styles.container}>
        <Header
          statusBarProps={{
            translucent: true,
            barStyle: 'light-content',
            backgroundColor: 'transparent',
          }}
          barStyle="light-content"
          // leftComponent={{ icon: 'menu', color: '#fff' }}
          leftComponent={<Logo />}
          rightComponent={<InfoButton _onPressInfo={() => navigate('Info')} />}
          containerStyle={styles.header}
        />
        <ScrollView style={styles.body}>
          <FlatList
            ItemSeparatorComponent={() => <View style={styles.separator} />}
            keyExtractor={this.keyExtractor}
            data={list}
            renderItem={this.renderItem}
          />
          <Text style={styles.announcement}>
            Скоро здесь будет больше медитаций!
          </Text>
        </ScrollView>
        <FooterAd stylext={styles.footer} />
      </View>
    )
  }
}

const Logo = () => (
  <View style={styles.logo}>
    <Image source={logo} style={styles.logoImg} />
    <Text style={styles.sublogo}>Практики осознанности</Text>
  </View>
)

const InfoButton = ({ _onPressInfo }) => (
  <View style={styles.infoView}>
    <TouchableOpacity onPress={_onPressInfo}>
      <Image source={infoIcon} style={styles.infoButton} />
    </TouchableOpacity>
  </View>
)

export const styles = StyleSheet.create({
  ...general,
  header: {
    height: 100,
    paddingBottom: 0,
    backgroundColor: backColorDark,
    borderBottomWidth: 0,
  },
  logo: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    width: 250,
    marginLeft: 5,
    marginTop: 5,
  },
  logoImg: {
    width: 256,
    height: 26,
  },
  sublogo: {
    marginBottom: 8,
    color: '#fff',
    fontFamily: 'Roboto-Light',
    fontSize: 14,
  },
  infoView: {
    flex: 1,
    paddingTop: 15,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  infoButton: {
    height: 32,
    width: 32,
    marginRight: 15,
  },
  listItem: {
    backgroundColor: backColorLight,
    // width: screenWidth * 0.9
  },
  separator: {
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: '#ccc',
    height: 1,
    width: screenWidth * 0.9,
  },
  announcement: {
    marginTop: 15,
    marginBottom: 15,
    textAlign: 'center',
    color: '#ccc',
    fontSize: 12,
  },
  body: {
    flex: 1,
    backgroundColor: backColorLight,
    // flexWrap: 'wrap',
    // /flexDirection: 'row',
    // justifyContent: 'space-around',
    // alignContent: 'flex-start',
  },
  footer: {
    height: 50,
    backgroundColor: '#eef',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
})
